import { StudentData } from './../studentData';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IChangedData } from '../studentData';

@Component({
  selector: 'app-student-marks',
  templateUrl: './student-marks.component.html',
  styleUrls: ['./student-marks.component.scss']
})
export class StudentMarksComponent implements OnInit {

  @Input() element!:StudentData;
  @Input() studentList:StudentData[]=[];
  // @Output() valueEmitter=new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

    updateScore(el:IChangedData){
      this.studentList.forEach(element=>{
       if(element.studentName===el.element.studentName){
         element.studentScore=el.value;
       }
       
      })
     }
   
}
