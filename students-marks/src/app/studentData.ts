export interface StudentData{
    studentName:string,
    studentScore:number
}

export interface IChangedData{
    value:number,
    element:StudentData
}