import { StudentData } from "../studentData";

export const studentsDataList=async():Promise<StudentData[]> =>{
    return [
        {
            studentName:'michael',
            studentScore:33
          },
          {
            studentName:'dwight',
            studentScore:44
          },
          {
            studentName:'jim',
            studentScore:11
          },
          {
            studentName:'pamela',
            studentScore:77
          },
          {
            studentName:'stanley',
            studentScore:55
          },
          {
            studentName:'creed',
            studentScore:55
          },
          {
            studentName:'oscar',
            studentScore:92
          },
          {
            studentName:'kevin',
            studentScore:69
          }
    ]
}