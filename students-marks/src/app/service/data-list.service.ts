import { StudentData } from './../studentData';
import { Injectable } from '@angular/core';
import { async } from '@angular/core/testing';
import { studentsDataList } from '../mock/backend';
import { IChangedData } from './../studentData';

@Injectable({
  providedIn: 'root'
})
export class DataListService {

  studentsList!:StudentData[];
  constructor() { }

  async getStudentData(){
    const studentsList=await studentsDataList();
    return studentsList;
  }

  // updateStudentData(item:IchangedData){
  //   let index= this.studentsList.indexOf(item.element);
  //   this.studentsList[index].studentScore=item.value;

  // }

}