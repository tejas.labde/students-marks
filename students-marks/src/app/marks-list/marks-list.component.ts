import { studentsDataList } from './../mock/backend';
import { DataListService } from './../service/data-list.service';
import { StudentData, IChangedData } from './../studentData';
import { Component, OnInit,Input } from '@angular/core';


@Component({
  selector: 'app-marks-list',
  templateUrl: './marks-list.component.html',
  styleUrls: ['./marks-list.component.scss']
})
export class MarksListComponent implements OnInit {

  data:StudentData[]=[];
  @Input() changedValue:number=0;

  constructor(private dataListService:DataListService) {}

  async ngOnInit(){
    this.data=await this.dataListService.getStudentData();
  }

  // updateScore(el:IChangedData){
  // //  this.data.forEach(element=>{
  // //   if(element.studentName===el.element.studentName){
  // //     element.studentScore=el.value;
  // //   }
    
  // //  })
  // this.data[el.index].studentScore=el.value;
  // }


}
